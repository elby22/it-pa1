package client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by Elby on 7/7/2016.
 */
public class Client {
    static String server;
    static int port;
    static String name;
    static Socket serverSocket = null;

    public static void main(String args[]){
        server = args[0];
        port = Integer.parseInt(args[1]);
        name = args[2];

        while(serverSocket == null){
            try {
                serverSocket = new Socket(server, port);
            } catch (IOException e) {
                System.out.println("Failed to connect, trying again in 3 seconds");
                serverSocket = null;
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

        try {
            OutputStream outToServer = serverSocket.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);

            out.writeUTF(name);

            InputStream inFromServer = serverSocket.getInputStream();
            DataInputStream in = new DataInputStream(inFromServer);

            String handshake = in.readUTF();
            System.out.println(handshake);

            if(handshake.charAt(0) == '!'){
                serverSocket.close();
                return;
            }

            //Spawn listener
            Listener L = new Listener(in);
            Thread listener = new Thread(L);
            listener.start();

            Scanner scanner = new Scanner(System.in);

            //Main loop to send server input from user.
            while(true){
                String message = scanner.nextLine();
                out.writeUTF(message);
                if(message.trim().equals("@exit")) exit();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Safely kills the client if it has been disconnected or if the user has requested it.
    public static void exit(){
        System.out.println("\033[0m Disconnected from server. Exiting");
        System.exit(0);
    }
}
