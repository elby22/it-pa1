package client;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Created by Elby on 7/7/2016.
 */
public class Listener implements Runnable{

    DataInputStream in;

    public Listener(DataInputStream in){
        this.in = in;
    }

    //Listens for responses from server and displays them to the screen. Calls client.exit if message to kill client is recieved.
    @Override
    public void run() {
        while(true){
            try {
                String message = in.readUTF();
                if(message.equals("@@@")) Client.exit();
                System.out.println(message);
            } catch (IOException e) {
                Client.exit();
                return;
            }
        }
    }
}
