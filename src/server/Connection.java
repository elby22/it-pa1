package server; /**
 * Created by elby on 7/2/16.
 */
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class Connection implements Runnable {
    Socket clientSocket;
    DataInputStream in;
    DataOutputStream out;
    int id;
    boolean inPrivate = false;
    String name;
    ArrayList<Connection> privateSession;

    //Constructor that takes a Socket from the main Server class.
    public Connection(Socket clientSocket){
        this.clientSocket = clientSocket;
        try {
            in = new DataInputStream(clientSocket.getInputStream());
            out = new DataOutputStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        privateSession = new ArrayList<>();
    }

    @Override
    public void run() {
        try {
            name = in.readUTF();
            id = Server.addUser(name, this);
            if(id == -1){
                out.writeUTF("!Too many users on this server, please try again later.");
            }else if(id == -2) {
                out.writeUTF("!A user by the name + '" + name + "' is on this server. Please reconnect with another name.");
            }else {
                Server.broadcast(0, name + " has connected from " + clientSocket.getInetAddress());
                out.writeUTF("\033[0mWelcome to this chatroom! Type in your message and press enter to chat.\n" +
                        "'@private <username>' will start a private session with that user.\n" +
                        "'@end <username>' will end a private session with that user.\n" +
                        "'@who' lists all active users in a session.\n" +
                        "'@exit' will disconnect from the server and kill this client program.\n" +
                        "Here is what you've missed so far:\n");
                Server.printHistory(this);
                System.out.println(name + " has connected from " + clientSocket.getInetAddress());
            }

            //Main loop handling input.
            while(true){
                try {
                    String input = in.readUTF();
                    String response = "";
                    if (input.charAt(0) == '@' ) {
                        if(input.substring(0,4).toLowerCase().equals("@end")){
                            input = input.trim().substring(5);
                            endPrivate(input);
                        }else if (input.substring(0, 4).toLowerCase().equals("@who")) {
                            send(Server.getUsers());

                        }else if (input.substring(0, 5).toLowerCase().equals("@exit")) {
                            disconnect();
                            return;
                        }else if (input.substring(0, 8).toLowerCase().equals("@private")) {
                            input = input.trim().substring(9);
                            startPrivate(input);
                        }else{
                            throw new StringIndexOutOfBoundsException();
                        }
                    }else{
                        if(privateSession.size() != 0){
                            input = Server.constructMessage(id, input);
                            for(Connection c : privateSession){
                                c.send(input);
                            }
                        }else{
                            Server.broadcast(id, input);
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException | StringIndexOutOfBoundsException e){
                    send(Server.constructMessage(0, "Invalid command. Try again"));
                } catch (EOFException eof){
                    disconnect();
                    return;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Sends a message to the client on the end of the instance of Client this method is called on.
    public synchronized void send(String message){
        try {
            out.writeUTF(message);
        } catch (IOException e) {
            disconnect();
        }
    }

    //Used to safely disconnect this instance of Connection's client from the server.
    private void disconnect(){

        //Remove users from private chat
        for(Connection c : privateSession){
            c.removeFromPrivateSilent(this);
        }

        Server.removeUser(name, id);
        send("@@@");
        try {
            clientSocket.close();
            in.close();
            out.close();
        } catch (Exception e) {
            //This thread is gonna die anyway - why say anything about it?
        }

        Server.broadcast(0, this.name + "(" + clientSocket.getInetAddress() + ") has been disconnected.");
        System.out.println(this.name + "(" + clientSocket.getInetAddress() + ") has been disconnected.");

    }

    //Helper method to initiate a private chat with the user in 'input'
    //Finds Connection instance from input by calling Server.getConnecton(), which is synchronized
    //Calls addToPrivate() on the Connection if found.
    private void startPrivate(String input){
        String name = input;
        Connection con = Server.getConnection(name);
        String send;
        if(con == null){
            send = Server.constructMessage(0, "Did not find a user by the name '" + name + "'.");
        }else{
            boolean success = con.addToPrivate(this);
            if(success){
                send = Server.constructMessage(0, name + " has joined your private session.");
                privateSession.add(con);
            }else{
                send = Server.constructMessage(0, name + " is already in a private session and cannot join.");
            }
        }
        send(send);
    }

    //Helper method to end a private chat with user in 'input'
    //calls removeFromPrivate() on the instance of that user's Connection if found.
    private void endPrivate(String input){
        String with = input;
        String send = "";
        if(privateSession.size() == 0){
            send = Server.constructMessage(0, "You are not in a private session.");
        }

        Connection end = null;
        for(Connection c : privateSession){
            if(c.name.toLowerCase().equals(with)){
                end = c;
                break;
            }
        }

        if(end == null){
            send = Server.constructMessage(0, "You are not in a private session with '" + with + "'.");
        }else{
            end.removeFromPrivate(this);
            privateSession.remove(end);
            send = Server.constructMessage(0, end.name + " has been removed from the private session.\n");

            if(privateSession.size() == 0)
                send += Server.constructMessage(0, "You are no longer in a private session. All following messages are public.");
        }
        send(send);
    }

    //Method to actually initiate private chat with user from the Connection parameter.
    //Called from c's instance on this instance.
    public synchronized boolean addToPrivate(Connection c){
        if(this.inPrivate){
            return false;
        }else{
            this.inPrivate = true;
            String send = Server.constructMessage(0, "You are now in a private session with " + c.name + "." +
                    "You will receive messages from this user that will not appear in the chatroom.");
            send(send);
            return true;
        }
    }

    //Method to actually end private chat with user from the Connection parameter.
    //Called from c's instance on this instance.
    public synchronized void removeFromPrivate(Connection c){
        this.inPrivate = false;
        String send = Server.constructMessage(0, "You have been removed from a private session with " + c.name + ".");
        send(send);
    }

    //Similar to the method above, but does so with no use of send() under the implication that the socket has been disconnected
    public synchronized void removeFromPrivateSilent(Connection c){
        this.inPrivate = false;
    }


}
