package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Server {

    static final int MAX_USERS = 20;
    static int currentUsers = 0;
    static int id = 1;
    static ServerSocket serverSocket;
    static ArrayList<String> chatHistory;

    //Maps a username to an id
    static HashMap<String, Integer> users;
    //Maps an id to an instance of Connection
    static HashMap<Integer, Connection> connections;

    //array of color escape sequences. A user's id is the index of the array so the color is consistent across a Connection's life cycle.
    static String[] colors =  {"\033[4m", //Messages from the server
                                "\033[0m",
                                "\033[31m",
                                "\033[32m",
                                "\033[33m",
                                "\033[34m",
                                "\033[35m",
                                "\033[36m",
                                "\033[37m",
                                "\033[7;31m",
                                "\033[7;32m",
                                "\033[7;33m",
                                "\033[7;34m",
                                "\033[7;35m",
                                "\033[7;36m",
                                "\033[7;37m",
                                "\033[1;47;31m",
                                "\033[1;47;32m",
                                "\033[1;47;33m",
                                "\033[1;47;34m",
                                "\033[1;47;36m"};

    public static void main(String[] args) {
        int portNumber = 6666;
        users = new HashMap<>(20);
        connections = new HashMap<>(20);

        try {
            serverSocket = new ServerSocket(portNumber);
            System.out.println("server.Server running on port "+ portNumber);

            chatHistory = loadHistory();
            if(chatHistory == null)
                chatHistory = new ArrayList<String>();
            else
                System.out.println("Loaded chat history from chat_history");

            //Accecpts new connections and spawns a thread to run a new instance of Connection
            while(currentUsers <= MAX_USERS) {
                Socket clientSocket = serverSocket.accept();

                Connection newConnection = new Connection(clientSocket);
                Thread thread = new Thread(newConnection);
                thread.start();
            }
        } catch(Exception e) {
            e.printStackTrace();
            return;
        }


    }

    //Safely adds a user by checking if the name exists, if not assigns an id and updates relevent data structures and vars.
    static synchronized int addUser(String name, Connection c){
        if(currentUsers >= MAX_USERS){
            return -1;
        }else if(users.containsKey(name.toLowerCase())){
            return -2;
        }else{
            if (id == 20) id = 1;
            else id++;
            while(users.containsValue(id)) {
                if (id == 20) id = 1;
                else id++;
            }
            users.put(name.toLowerCase(), id);
            connections.put(id, c);
            currentUsers++;
            return id;
        }
    }

    //Safely removes a user from the chatroom by updating the relevent data structures and vars.
    static synchronized void removeUser(String name, int id){
        users.remove(name.toLowerCase());
        connections.remove(id);
        currentUsers--;
    }

    //Used to send a message to all users in the chatroom from a single user.
    //Also adds the message to the history
    static synchronized void broadcast(int id, String message){
        String text = constructMessage(id, message);
        chatHistory.add(text);
        serialize(chatHistory);
        if(connections == null) return;
        Iterator i = connections.entrySet().iterator();
        while(i.hasNext()){
            Map.Entry entry = (Map.Entry)i.next();
            Connection connection = (Connection)entry.getValue();
            connection.send(text);
        }
    }

    //Safely sends the chat history to a client. Called when a new user enters the chat.
    static synchronized void printHistory(Connection c){
        for(String s : chatHistory){
            c.send(s);
        }
    }

    //Builds a message string from the user id and a message by formatting with color and name.
    static synchronized String constructMessage(int id, String message){
        String name = "Server";
        if(id != 0) {
            Connection c = connections.get(id);
            name = c.name;
        }
        String text = colors[id] + name + ": " + message + "\033[0m"; //End part clears formatting
        return text;
    }

    //Safely gets the active users in the chatroom and their statuses. Returns a formatted string to be sent to the client requesting the information.
    static synchronized String getUsers(){
        String response = constructMessage(0, "Active users in this chatroom:\n");
        Iterator i = users.entrySet().iterator();
        while(i.hasNext()){
            Map.Entry entry = (Map.Entry)i.next();
            int id = (Integer)entry.getValue();
            String status = "";
            if(connections.get(id).inPrivate) status += " In private session";
            else status = " In public chatroom";

            response += constructMessage(id, status + "\n");
        }
        return response;
    }

    //Safely finds an instance of Connection for the username in the parameters.
    static public synchronized Connection getConnection(String name){
        if (users.containsKey(name.toLowerCase())){
            return connections.get(users.get(name.toLowerCase()));
        }else{
            return null;
        }
    }

    //Serializes the history. Called from the already synchronized method broadcast().
    static void serialize(ArrayList<String> history){
        try{
            FileOutputStream fos = new FileOutputStream("chat_history");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(history);
            oos.close();
            fos.close();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }

    //Loads and returns chat history from a file 'chat_history', if found.
    static ArrayList<String> loadHistory(){
        try{
            FileInputStream fis = new FileInputStream("chat_history");
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<String> history = (ArrayList) ois.readObject();
            ois.close();
            fis.close();
            return history;
        }catch(IOException ioe){
            System.out.println("No file 'chat_history' found; no history loaded");
            return null;
        }catch(ClassNotFoundException c){
            System.out.println("Corrupted history file; not loaded.");
            return null;
        }
    }
}
