# Multithreaded Chat Room
 #Rutgers CS 352 
Project 1

## Abstract ##
The two components of this application combine to create a chatroom that communicates over TCP. The server simply runs and accepts connections from instances of the client. The chatroom has many features which are outlined in the program specification and the entire application was developed in Java. This application was designed to take advantage of Java’s multithreading features which allows for many (limited to 20 in this case) connections concurrently using shared resources on the server without any data corruption. 

## Design ##
There are two distinct runnable classes in the application suite: Server.class from the server package, and Client.class from the client package. The server does all of the work in terms of processing commands and formatting output whereas the client simply takes input from the user and prints messages from the server. The server runs on port 6666.
Server package
### Server ###:
	Contains the main method for the server application. This class has static data members and methods to handle adding and removing users, chat history, serialization, message switching for public chatroom and getting information about server/user status for clients. These methods are ‘synchronized’ so as to maintain data consistency when the data is accessed and changed from a concurrent context. Because these methods are static, there is only one instance of each data member and the synchronization of each method to access these resources locks the data for the calling object (mostly called from an instance of Connection). More information about the data members and methods are detailed in the code comments.
	The main method open a socket on port 6666, loads chat history if it exists and listens for new connections. If a new connection is made, it creates a new instance of Connection and delegates the management of that client to a new thread. 

### Connection ###:
	A runnable class that handles a connection to a client. Contains data members for tracking username and id as well as private session data and client I/O. A new thread is created from the server class upon a new connection from a client, which executes run(). A loop to handle data received from the client runs in this method until the client disconnects. Other instance methods control the Connection’s communication with the main Server class (and thus other clients) and sending information to that client. More information about methods and the Connection class can be found in the code comments.

## Client Package
 ##### Client ###:
	Contains the main method for the client application. Takes 3 arguments from command line: hostname, port and username. The program parses arguments and tries to establish a connection to the server every 3 seconds until successful. Upon connection, an instance of Listener is run in a new thread to listen to replies from the server. The run() method continues in a loop to take input from the user and send it to the server until the user quits.

### Listener ###:
	This is a very simple class: it runs a loop to take messages from the server and print them to the client’s console. It does this in a separate thread spawned from Client.
Building and Execution
Building and running java programs from the shell requires knowing classpaths, directories and packages. To simplify the execution of the programs, the makefile compiles jars. These programs are JVM 1.7 compliant, which is the java version running on the ILab cluster.

### The Makefile targets:
 ###	$make server: builds the server app as Server.jar
	$make client: builds the client app as Client.jar
	$make clean: removes .jars
	$make or $make default: builds both .jars
	
### To run the server: 
 ###	$java -jar Server.jar

### To run the client: 
 ###$java -jar Client.jar <hostname> 6666 <username>

Notes:
The @name command has been omitted because the username is supplied by the arguments.
 