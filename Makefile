default: server client

server: src/server/Server.java src/server/Connection.java
	javac -g -d . src/server/*.java
	jar cmvf src/server/META-INF/MANIFEST.MF Server.jar server/Server.class server/Connection.class
	rm -r server/

client: src/client/Client.java src/client/Listener.java
	javac -g -d . src/client/*.java
	jar cmvf src/client/META-INF/MANIFEST.MF Client.jar client/Client.class client/Listener.class
	rm -r client/

clean:
	rm *.jar
	rm chat_history
